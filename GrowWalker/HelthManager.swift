//
//  HelthManager.swift
//  GrowWalker
//
//  Created by 이재득 on 2016. 6. 27..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import HealthKit

class HealthKitManager {
    
    let healthKitStore: HKHealthStore = HKHealthStore()
    
    func authorizeHealthKit(completion: ((success: Bool, error: NSError!) -> Void)!) {
        
        // State the health data type(s) we want to read from HealthKit.
        let healthDataToRead = Set(arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!, HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!)
        
        // State the health data type(s) we want to write from HealthKit.
        let healthDataToWrite = Set(arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!, HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!)
        
        // Just in case OneHourWalker makes its way to an iPad...
        if !HKHealthStore.isHealthDataAvailable() {
            print("Can't access HealthKit.")
        }
        
        // Request authorization to read and/or write the specific data.
        healthKitStore.requestAuthorizationToShareTypes(healthDataToWrite, readTypes: healthDataToRead) { (success, error) -> Void in
            if( completion != nil ) {
                completion(success:success, error:error)
            }
        }
    }
    func queryDailyStats(metric: NSString!, handler: ([NSDate: Int] -> Void)!) {
        let quantityType = HKObjectType.quantityTypeForIdentifier(metric as String)
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        let preservedComponents: NSCalendarUnit = [.Year , .Month , .Day]
        let midnight: NSDate! = calendar.dateFromComponents(calendar.components(preservedComponents, fromDate:now))
        let dailyInterval = NSDateComponents()
        dailyInterval.day = 1
        
        let tomorrow = calendar.dateByAddingUnit(.Month, value: 1, toDate: midnight, options: [])
        let oneMonthAgo = calendar.dateByAddingUnit(.Month, value: -1, toDate: midnight, options: [])
        let oneWeekAgo = calendar.dateByAddingUnit(.Day, value: -6, toDate: midnight, options: []) // only need to start from 6 days back
        
        // need tomorrow's midnight as end date to get all of today's data
        let predicate = HKQuery.predicateForSamplesWithStartDate(oneWeekAgo, endDate: tomorrow, options: .None)
        
        let query = HKStatisticsCollectionQuery(
            quantityType: quantityType!,
            quantitySamplePredicate: predicate,
            options: .CumulativeSum,
            anchorDate: midnight,
            intervalComponents: dailyInterval
        )
        
        query.initialResultsHandler = { query, results, error -> Void in
            var data:[NSDate: Int] = [:]
            if error != nil {
                print(error)
            } else {
                results!.enumerateStatisticsFromDate(oneWeekAgo!, toDate: tomorrow!) { statistics, stop in
                    if let quantity = statistics.sumQuantity() {
                        let date = statistics.startDate
                        let value = Int(quantity.doubleValueForUnit(HKUnit.countUnit()))
                        
                        data[date] = value
                    }
                }
            }
            
            handler(data)
        }
        
        self.healthKitStore.executeQuery(query)
    }
    func recentSteps(completion: ((HKSample!, NSError!) -> Void)!)
    {
        let locale:NSLocale = NSLocale.currentLocale()
        // The type of data we are requesting (this is redundant and could probably be an enumeration
        let type = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!
        
        let calendar = NSCalendar.currentCalendar()
        calendar.locale = locale
        let currentDate = NSDate()
        //        let yesterday = calendar.dateByAddingUnit(.Day, value: -1, toDate: currentDate, options: [])! as NSDate
        let year = 1970
        let month = 1
        let day = 1
        
        let yesterday = makeDate(year, month: month, day: day)
        
        
        // Our search predicate which will fetch data from now until a day ago
        // (Note, 1.day comes from an extension
        // You'll want to change that to your own NSDate
        let predicate = HKQuery.predicateForSamplesWithStartDate (yesterday, endDate: NSDate(), options: .None)
        
        // The actual HealthKit Query which will fetch all of the steps and sub them up for us.
        let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastSteps = results!.last!
            
            if completion != nil {
                completion(lastSteps, nil)
            }
        }
        
        self.healthKitStore.executeQuery(query)
    }
    func makeDate(year:Int, month:Int, day:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        dateComponents.nanosecond = 0
        let date = calendar.dateFromComponents(dateComponents)
        return date!
    }
    func makeDateTime(year:Int, month:Int, day:Int, hour:Int, minute:Int, second:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        dateComponents.nanosecond = 0
        
        let datetime = calendar.dateFromComponents(dateComponents)
        return datetime!
    }
    func getSteps(startDate:NSDate, sampleType: HKSampleType , completion: ((HKQuantity!, NSError!) -> Void)!) {
        // Predicate for the height query
        let currentDate = NSDate()
        
        let yesterday = startDate
        
        
        let lastStepPredicate = HKQuery.predicateForSamplesWithStartDate(yesterday, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let stepQuery = HKSampleQuery(sampleType: sampleType, predicate: lastStepPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            let quantity:HKQuantity
            var steps: Double = 0
            if results?.count > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    steps += result.quantity.doubleValueForUnit(HKUnit.countUnit())
                }
                quantity = HKQuantity.init(unit: HKUnit.countUnit(), doubleValue: steps)
                
                let dayStep = quantity
                if completion != nil {
                    completion(dayStep, nil)
                }

            } else {
                quantity = HKQuantity.init(unit: HKUnit.countUnit(), doubleValue: 0)
                completion(quantity, error)
                return
            }
            // Set the first HKQuantitySample in results as the most recent height.
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(stepQuery)
    }
    func getHeight(sampleType: HKSampleType , completion: ((HKSample!, NSError!) -> Void)!) {
        
        // Predicate for the height query
        let distantPastHeight = NSDate.distantPast() as NSDate
        let currentDate = NSDate()
        let lastHeightPredicate = HKQuery.predicateForSamplesWithStartDate(distantPastHeight, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let heightQuery = HKSampleQuery(sampleType: sampleType, predicate: lastHeightPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastHeight = results!.first
            
            if completion != nil {
                completion(lastHeight, nil)
            }
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(heightQuery)
    }
    
    func saveDistance(distanceRecorded: Double, date: NSDate ) {
        
        // Set the quantity type to the running/walking distance.
        let distanceType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)
        
        // Set the unit of measurement to miles.
        let distanceQuantity = HKQuantity(unit: HKUnit.mileUnit(), doubleValue: distanceRecorded)
        
        // Set the official Quantity Sample.
        let distance = HKQuantitySample(type: distanceType!, quantity: distanceQuantity, startDate: date, endDate: date)
        
        // Save the distance quantity sample to the HealthKit Store.
        healthKitStore.saveObject(distance, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print(error)
            } else {
                print("The distance has been recorded! Better go check!")
            }
        })
    }
}