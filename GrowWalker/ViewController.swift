//
//  ViewController.swift
//  Steps
//
//  Created by Shrikar Archak on 4/11/15.
//  Copyright (c) 2015 Shrikar Archak. All rights reserved.
//

import UIKit
import HealthKit



class ViewController: UIViewController {
    
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    let healthManager:HealthKitManager = HealthKitManager()
    var height: HKQuantitySample?
    var step: HKQuantity?
    
    
    let healthStore: HKHealthStore? = {
        if HKHealthStore.isHealthDataAvailable() {
            return HKHealthStore()
        } else {
            return nil
        }
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib
        getHealthKitPermission()
        self.getStep()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHealthKitPermission() {
        
        // Seek authorization in HealthKitManager.swift.
        healthManager.authorizeHealthKit { (authorized,  error) -> Void in
            if authorized {
                
                // Get and set the user's height.
                self.setHeight()
                self.getStep()
            } else {
                if error != nil {
                    print(error)
                }
                print("Permission denied.")
            }
        }
    }

    
    func getStep() {
        // Create the HKSample for Step.
        let stepSample = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)
        
//        self.healthManager.recentSteps({ (steps, error) -> Void in
//            var countsString = ""
//            let step = steps as? HKQuantitySample
//            let foramtCounts = NSNumberFormatter()
//            countsString = foramtCounts.stringFromNumber((step?.quantity.doubleValueForUnit(HKUnit.countUnit()))!)!
//            
//            // Set the label to reflect the user's height.
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                self.stepLabel.text = countsString
//            })
//        })
        
        let startDate = NSDate()
        self.healthManager.getSteps(startDate, sampleType: stepSample!, completion:  { (steps, error) -> Void in
            
            if( error != nil ) {
                print("Error: \(error.localizedDescription)")
                return
            }
            
            var countsString = ""
            var a:Int
            
//            print(steps)
            self.step = steps
            
            // The height is formatted to the user's locale.
            if (self.step != nil){
                let formatCounts = NSNumberFormatter()
                countsString = formatCounts.stringFromNumber(self.step!.doubleValueForUnit(HKUnit.countUnit()))!
            }
            print(countsString)
            // Set the label to reflect the user's height.
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.stepLabel.text = countsString
            })
        })
        
    }
    func setHeight() {
        // Create the HKSample for Height.
        let heightSample = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        
        // Call HealthKitManager's getSample() method to get the user's height.
        self.healthManager.getHeight(heightSample!, completion: { (userHeight, error) -> Void in
            
            if( error != nil ) {
                print("Error: \(error.localizedDescription)")
                return
            }
            
            var heightString = ""
            
            self.height = userHeight as? HKQuantitySample
            
            // The height is formatted to the user's locale.
            if let meters = self.height?.quantity.doubleValueForUnit(HKUnit.meterUnit()) {
                let formatHeight = NSLengthFormatter()
                formatHeight.forPersonHeightUse = true
                heightString = formatHeight.stringFromMeters(meters)
            }
            
            // Set the label to reflect the user's height.
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.heightLabel.text = heightString
            })
        })
        
    }
    
    
}